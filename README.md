# Kotlin Spring Security Backend

### Things to do this repo locally

#### Clone the repo:
```
git clone https://gitlab.com/hendisantika/kotlin-spring-security-backend.git
```

#### Go to the folder:
```
cd kotlin-spring-security-backend
```

#### Run the app:
```
gradle clean bootRun --info
```

### Screen shot

Login Page

![Login Page](img/login.png "Login Page")

Home Page

![Home Page](img/home.png "Home Page")

Logout Page

![Logout Page](img/logout1.png "Logout page")

![Logout Page](img/logout2.png "Logout page")